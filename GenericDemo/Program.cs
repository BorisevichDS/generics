﻿using System;
using System.Collections.Generic;

namespace GenericDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string quitAnswer = "N";

                RoadMap<Node<City>, City> road = new RoadMap<Node<City>, City>();
                InitializeRoadMap(road);

                Dictionary<byte, string> questions = new Dictionary<byte, string>();
                questions.Add(1, "Вывести список городов");
                questions.Add(2, "Найти кратчайшее расстояние между гороадами.");

                Console.WriteLine("Что вы хотите сделать? (Укажите число)");
                foreach (var el in questions)
                {
                    Console.WriteLine(el);
                }

                do
                {
                    Console.Write("Введите выбранный вариант: ");
                    string inputString = Console.ReadLine().Trim();
                    if (int.TryParse(inputString, out int variant))
                    {
                        if (variant == 1)
                        {
                            foreach (var c in road.Nodes)
                            {
                                Console.WriteLine(c.Data);
                            }
                        }
                        else if (variant == 2)
                        {
                            Console.Write("Введите начальный город: ");
                            string cityFrom = Console.ReadLine();

                            Console.Write("Введите конечный город: ");
                            string cityTo = Console.ReadLine();

                            string path = road.FindShortestPath(cityFrom, cityTo);
                            Console.WriteLine(path);
                        }
                        else
                        {
                            Console.WriteLine("Указанного варианта не существует.");

                        }
                    }
                    Console.Write("Хотите продолжить? (Y - да, N - нет): ");
                    quitAnswer = Console.ReadLine().Trim().ToUpper();
                }
                while (quitAnswer.Equals("Y"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            Console.ReadKey();
        }

        /// <summary>
        /// Метод заполняет граф данными.
        /// </summary>
        /// <param name="roadMap">Граф, который нужно наполнить данными.</param>
        private static void InitializeRoadMap(RoadMap<Node<City>, City> roadMap)
        {
            ICityRepository cityRepository = new CityRepository();

            foreach (var city in cityRepository.GetCities())
            {
                roadMap.Add(new Node<City>(city));
            }

            foreach (var cityDistance in cityRepository.GetDistances())
            {
                var nodeFrom = roadMap.GetNodeByData(cityDistance.From);
                var nodeTo = roadMap.GetNodeByData(cityDistance.To);
                Edge<City> edge = new Edge<City>(nodeFrom, nodeTo, cityDistance.Distance);

                roadMap.AddEdge(edge);
            }

        }
    }
}
