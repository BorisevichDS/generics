﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace GenericDemo
{
    /// <summary>
    /// Предоставляет информацию о городах.
    /// </summary>
    internal class CityRepository : ICityRepository
    {
        private IConfigurationRoot _configurationRoot;

        /// <summary>
        /// Список городов.
        /// </summary>
        private List<City> _cities;

        /// <summary>
        /// Список расстояний между городами.
        /// </summary>
        private List<CityDistance> _distances;

        public CityRepository()
        {
            _configurationRoot = new ConfigurationBuilder()
                                .SetBasePath(Directory.GetCurrentDirectory())
                                .AddJsonFile("AppSettings.json")
                                .Build();

            _cities = new List<City>();
            _distances = new List<CityDistance>();
            ParseCity();
            ParseCityDistance();
        }

        /// <summary>
        /// Метод возвращает путь к CSV файлу.
        /// </summary>
        /// <returns></returns>
        private string GetPath() => _configurationRoot.GetSection("CSVFilePath").Value;

        /// <summary>
        /// Метод парсит CSV файл и наполняет коллекцию _cities.
        /// </summary>
        private void ParseCity()
        {
            string result = string.Empty;

            using (StreamReader sr = new StreamReader(GetPath(), Encoding.UTF8))
            {
                result = sr.ReadLine();
            }

            foreach (var city in result.Split(';'))
            {
                if (!string.IsNullOrEmpty(city))
                {
                    _cities.Add(new City(city));
                }
            }

        }

        /// <summary>
        /// Метод парсит CSV файл и наполняет коллекцию _distances.
        /// </summary>
        private void ParseCityDistance()
        {
            using (StreamReader sr = new StreamReader(GetPath(), Encoding.UTF8))
            {
                // Разбиваем текс на строки
                string[] lines = sr.ReadToEnd().Split("\n");

                // Парсим строки
                for (int i = 1; i < lines.Length; i++)
                {
                    string[] elements = lines[i].Split(";");

                    for (int j = 1; j < elements.Length; j++)
                    {
                        int.TryParse(elements[j], out int distance);
                        CityDistance cityDistance = new CityDistance(_cities[i-1], _cities[j-1], distance);
                        _distances.Add(cityDistance);
                    }
                }
            }
        }

        /// <summary>
        /// Метод возвращает список городов.
        /// </summary>
        /// <returns></returns>
        public List<City> GetCities() => _cities;

        /// <summary>
        /// Метод возвращает расстояния между городами.
        /// </summary>
        /// <returns></returns>
        public List<CityDistance> GetDistances() => _distances;
    }
}
