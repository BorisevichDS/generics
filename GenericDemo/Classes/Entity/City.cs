﻿using System;

namespace GenericDemo
{
    /// <summary>
    /// Город.
    /// </summary>
    internal class City
    {
        /// <summary>
        /// Название города.
        /// </summary>
        public string Name { get; }

        public City(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
