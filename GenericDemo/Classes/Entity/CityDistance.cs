﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericDemo
{
    /// <summary>
    /// Расстояние между городами.
    /// </summary>
    internal class CityDistance
    {
        /// <summary>
        /// Начальный город.
        /// </summary>
        public City From { get; }

        /// <summary>
        /// Конечный город.
        /// </summary>
        public City To { get;  }

        /// <summary>
        /// Расстояние между городами.
        /// </summary>
        public int Distance { get; }

        public CityDistance(City from, City to, int distance)
        {
            From = from;
            To = to;
            Distance = distance;
        }
    }
}
