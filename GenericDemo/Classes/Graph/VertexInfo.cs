﻿using System;

namespace GenericDemo
{
    /// <summary>
    /// Информация о вершине.
    /// </summary>
    internal class VertexInfo<TNode, TNodeType> where TNode: INode<TNodeType>
    {
        /// <summary>
        /// Вершина.
        /// </summary>
        public INode<TNodeType> Vertex { get; set; }

        /// <summary>
        /// Предыдущая вершина.
        /// </summary>
        public INode<TNodeType> PreviousVertex { get; set; }

        /// <summary>
        /// Не посещенная вершина.
        /// </summary>
        public bool IsUnvisited { get; set; }

        /// <summary>
        /// Сумма весов ребер.
        /// </summary>
        public int EdgesWeightSum { get; set; }

        public VertexInfo(INode<TNodeType> vertex)
        {
            Vertex = vertex;
            PreviousVertex = null;
            IsUnvisited = true;
            EdgesWeightSum = int.MaxValue;
        }

        public override string ToString()
        {
            return $"Previous vertex: {PreviousVertex}, vertex: {Vertex}, EdgesWeightSum: {EdgesWeightSum}";
        }
    }
}
