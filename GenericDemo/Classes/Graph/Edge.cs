﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericDemo
{
    /// <summary>
    /// Ребро графа.
    /// </summary>
    /// <typeparam name="TNodeType">Тип узла.</typeparam>
    internal class Edge<TNodeType> : IEdge<TNodeType>
    {
        /// <summary>
        /// Начальный узел.
        /// </summary>
        public INode<TNodeType> FromNode { get; }

        /// <summary>
        /// Конечный узел.
        /// </summary>
        public INode<TNodeType> ToNode { get; }

        /// <summary>
        /// Вес.
        /// </summary>
        public int Weight { get; }

        public Edge(INode<TNodeType> fromNode, INode<TNodeType> toNode, int weight)
        {
            FromNode = fromNode;
            ToNode = toNode;
            Weight = weight;
        }

        public override string ToString()
        {
            return $"{FromNode} - {ToNode} ({Weight})";
        }
    }
}
