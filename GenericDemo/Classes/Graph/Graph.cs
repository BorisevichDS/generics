﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GenericDemo
{
    /// <summary>
    /// Граф.
    /// </summary>
    /// <typeparam name="TNode">Узел.</typeparam>
    /// <typeparam name="TNodeType">Тип узла.</typeparam>
    internal class Graph<TNode, TNodeType> : IGraph<TNode, TNodeType> where TNode: INode<TNodeType>
    {
        /// <summary>
        /// Список узлов.
        /// </summary>
        public List<TNode> Nodes { get; }

        /// <summary>
        /// Список ребер.
        /// </summary>
        public List<IEdge<TNodeType>> Edges { get; }

        public Graph()
        {
            Nodes = new List<TNode>();
            Edges = new List<IEdge<TNodeType>>();
        }

        /// <summary>
        /// Метод добавляет узел в граф.
        /// </summary>
        /// <param name="node">Узел.</param>
        /// <returns></returns>
        public void Add(TNode node) => Nodes.Add(node);

        /// <summary>
        /// Метод добавляет массив узлов в граф.
        /// </summary>
        /// <param name="nodes"></param>
        public void AddRange(TNode[] nodes)
        {
            foreach (var n in nodes)
            {
                Nodes.Add(n);
            }
        }

        /// <summary>
        /// Метод задает связь (ребро) между двумя узлами.
        /// </summary>
        /// <param name="edge">Ребро.</param>
        public void AddEdge(IEdge<TNodeType> edge) => Edges.Add(edge);

        /// <summary>
        /// Метод задает связи (ребра) между узлами.
        /// </summary>
        /// <param name="edges">Массив ребер.</param>
        public void AddEdgeRange(IEdge<TNodeType>[] edges)
        {
            foreach (var e in edges)
            {
                Edges.Add(e);
            }
        }

        /// <summary>
        /// Метод проверяет наличие узла в графе.
        /// </summary>
        /// <param name="id">Идентификатор узла.</param>
        public bool Contains(int id) => Nodes.Exists(el => el.Id.Equals(id));

        /// <summary>
        /// Метод проверяет наличие узла в графе.
        /// </summary>
        /// <param name="node">Узел, который необходимо найти.</param>
        public bool Contains(TNode node) => Nodes.Exists(el => el.Id.Equals(node.Id));

        /// <summary>
        /// Метод удаляет узел из графа.
        /// </summary>
        /// <param name="id">Идентификатор узла.</param>
        public void Remove(int id)
        {
            TNode node = Nodes.Find(el => el.Id.Equals(id));
            Nodes.Remove(node);
        }

        /// <summary>
        /// Метод удаляет узел из графа.
        /// </summary>
        /// <param name="node">Узел, который необходимо удалить.</param>
        public void Remove(TNode node) => Nodes.Remove(node);

        /// <summary>
        /// Метод возвращает узел по его id.
        /// </summary>
        /// <param name="id">Идентификатор узла.</param>
        /// <returns></returns>
        public TNode GetNodeById(int id) => Nodes.First(el => el.Id.Equals(id));

        /// <summary>
        /// Метод возвращает узел по его данным.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public TNode GetNodeByData(TNodeType data) => Nodes.FirstOrDefault(n => n.Data.Equals(data));
    }
}
