﻿using System;

namespace GenericDemo
{
    /// <summary>
    /// Узел графа.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class Node<T> : INode<T>
    {
        /// <summary>
        /// Идентификатор узла.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Данные узла.
        /// </summary>
        public T Data { get; }

        public Node(T data)
        {
            Random random = new Random();
            Id = random.Next();
            Data = data;
        }

        public Node(int id, T data)
        {
            Id = id;
            Data = data;
        }

        public override string ToString()
        {
            return Data.ToString();
        }
    }
}
