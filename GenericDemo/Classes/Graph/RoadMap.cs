﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace GenericDemo
{
    internal class RoadMap<TNode, TNodeType> : Graph<TNode, TNodeType> where TNode: INode<TNodeType>
                                                                       where TNodeType: City
    {
        private List<VertexInfo<TNode, TNodeType>> _vertexInfoList = new List<VertexInfo<TNode, TNodeType>>();

        /// <summary>
        /// Метод инициализирует список объектов с информацией о вершинах.
        /// </summary>
        private void IntitializeVertexInfo()
        {
            foreach (var el in Nodes)
            {
                _vertexInfoList.Add(new VertexInfo<TNode, TNodeType>(el));
            }
        }

        /// <summary>
        /// Метод ищет непосещенную вершину с минимальным значением суммы весов ребер.
        /// </summary>
        /// <returns>Информация о вершине</returns>
        private VertexInfo<TNode, TNodeType> FindUnvisitedVertexWithMinSum()
        {
            var minValue = int.MaxValue;
            VertexInfo<TNode, TNodeType> minVertexInfo = null;
            foreach (var i in _vertexInfoList)
            {
                if (i.IsUnvisited && i.EdgesWeightSum < minValue)
                {
                    minVertexInfo = i;
                    minValue = i.EdgesWeightSum;
                }
            }
            return minVertexInfo;
        }

        /// <summary>
        /// Метод возвращает информацию о вершине.
        /// </summary>
        /// <param name="vertex">Вершина, о которой необходимо получить информацию.</param>
        /// <returns></returns>
        private VertexInfo<TNode, TNodeType> GetVertexInfo(INode<TNodeType> vertex)
        {
            return _vertexInfoList.FirstOrDefault(vi => vi.Vertex.Equals(vertex));
        }

        /// <summary>
        /// Метод вычисляет суммы весов ребер для следующей вершины.
        /// </summary>
        /// <param name="vertex">Информация о текущей вершине</param>
        private void SetSumToNextVertex(VertexInfo<TNode, TNodeType> vertex)
        {
            vertex.IsUnvisited = false;
            foreach (IEdge<TNodeType> e in Edges.Where(ed => ed.FromNode.Equals(vertex.Vertex)))
            {
                var nextInfo = GetVertexInfo(e.ToNode);
                var sum = vertex.EdgesWeightSum + e.Weight;
                if (sum < nextInfo.EdgesWeightSum)
                {
                    nextInfo.EdgesWeightSum = sum;
                    nextInfo.PreviousVertex = vertex.Vertex;
                }
            }
        }

        /// <summary>
        /// Метод формирует путь.
        /// </summary>
        /// <param name="fromVertex">Начальная вершина</param>
        /// <param name="toVertex">Конечная вершина</param>
        /// <returns>Путь</returns>
        private string GetPath(INode<TNodeType> fromVertex, INode<TNodeType> toVertex)
        {
            StringBuilder path = new StringBuilder();

            var toVertexInfo = GetVertexInfo(toVertex);
            path.Append($"Общее наикратчайщее расстояние между городами {fromVertex.Data.Name} и {toVertex.Data.Name} равно {toVertexInfo.EdgesWeightSum}.");
            path.Append(Environment.NewLine);
            path.Append("Детализация пути: ");

            int position = path.Length;

            // Пробегаем по вершинам от конечной до начальной. 
            do
            {
                // Так как оперируем суммой весов ребер, то нужно хранить информацию о текущей и предыдущей вершинах.
                VertexInfo<TNode, TNodeType> currentVertexInfo = toVertexInfo;

                toVertex = toVertexInfo.PreviousVertex;
                toVertexInfo = GetVertexInfo(toVertex);

                path.Insert(position, $"{toVertexInfo.Vertex} - {currentVertexInfo.Vertex} ({currentVertexInfo.EdgesWeightSum - toVertexInfo.EdgesWeightSum}) ");
            }
            while (fromVertex != toVertex);

            return path.ToString().Trim();
        }

        /// <summary>
        /// Поиск кратчайшего между вершинами.
        /// </summary>
        /// <param name="from">Начальная вершина</param>
        /// <param name="to">Конечная вершина</param>
        /// <returns>Кратчайший путь</returns>
        public string FindShortestPath(string from, string to)
        {
            IntitializeVertexInfo();

            INode<TNodeType> fromVertex = Nodes.FirstOrDefault(n => n.Data.Name.Equals(from));

            if (fromVertex == null)
                return $"Город {from} не найден.";

            INode<TNodeType> toVertex = Nodes.FirstOrDefault(n => n.Data.Name.Equals(to));

            if (toVertex == null)
                return $"Город {to} не найден.";

            var first = GetVertexInfo(fromVertex);
            first.EdgesWeightSum = 0;

            VertexInfo<TNode, TNodeType> current;
            do
            {
                current = FindUnvisitedVertexWithMinSum();

                if (current != null)
                    SetSumToNextVertex(current);

            }
            while (current != null);

            return GetPath(fromVertex, toVertex);
        }
    }
}
