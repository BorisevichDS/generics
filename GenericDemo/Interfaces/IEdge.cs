﻿using System;

namespace GenericDemo
{
    /// <summary>
    /// Интерфейс для ребра графа.
    /// </summary>
    /// <typeparam name="TNodeType">Тип узла.</typeparam>
    internal interface IEdge<TNodeType>
    {
        /// <summary>
        /// Начальный узел.
        /// </summary>
        INode<TNodeType> FromNode { get; }

        /// <summary>
        /// Конечный узел.
        /// </summary>
        INode<TNodeType> ToNode { get; }

        /// <summary>
        /// Вес.
        /// </summary>
        int Weight { get; }
    }
}
