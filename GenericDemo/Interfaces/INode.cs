﻿using System;

namespace GenericDemo
{
    /// <summary>
    /// Интерфейс узла графа.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal interface INode<T>
    {
        /// <summary>
        /// Идентификатор узла.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Данные узла.
        /// </summary>
        T Data { get; }
    }
}