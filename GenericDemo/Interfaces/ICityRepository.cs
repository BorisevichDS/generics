﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericDemo
{
    /// <summary>
    /// Репозитоий городов.
    /// </summary>
    internal interface ICityRepository
    {
        /// <summary>
        /// Метод возвращает список городов.
        /// </summary>
        /// <returns></returns>
        List<City> GetCities();

        /// <summary>
        /// Метод возвращает расстояния между городами.
        /// </summary>
        /// <returns></returns>
        List<CityDistance> GetDistances();
    }
}
