﻿using System;
using System.Collections.Generic;

namespace GenericDemo
{
    /// <summary>
    /// Интерфейс для графа.
    /// </summary>
    /// <typeparam name="TNode">Узел.</typeparam>
    /// <typeparam name="TNodeType">Тип узла.</typeparam>
    internal interface IGraph<TNode, TNodeType> where TNode: INode<TNodeType>
    {
        /// <summary>
        /// Список узлов.
        /// </summary>
        List<TNode> Nodes { get; }

        /// <summary>
        /// Список ребер.
        /// </summary>
        List<IEdge<TNodeType>> Edges { get; }

        /// <summary>
        /// Метод добавляет узел в граф.
        /// </summary>
        /// <param name="node">Узел.</param>
        void Add(TNode node);

        /// <summary>
        /// Метод добавляет массив узлов в граф.
        /// </summary>
        /// <param name="nodes"></param>
        void AddRange(TNode[] nodes);

        /// <summary>
        /// Метод задает связь (ребро) между двумя узлами.
        /// </summary>
        /// <param name="edge">Ребро.</param>
        void AddEdge(IEdge<TNodeType> edge);

        /// <summary>
        /// Метод задает связи (ребра) между узлами.
        /// </summary>
        /// <param name="edges">Массив ребер.</param>
        void AddEdgeRange(IEdge<TNodeType>[] edges);

        /// <summary>
        /// Метод проверяет наличие узла в графе.
        /// </summary>
        /// <param name="id">Идентификатор узла.</param>
        bool Contains(int id);

        /// <summary>
        /// Метод проверяет наличие узла в графе.
        /// </summary>
        /// <param name="node">Узел, который необходимо найти.</param>
        bool Contains(TNode node);

        /// <summary>
        /// Метод удаляет узел из графа.
        /// </summary>
        /// <param name="id">Идентификатор узла.</param>
        void Remove(int id);

        /// <summary>
        /// Метод удаляет узел из графа.
        /// </summary>
        /// <param name="node">Узел, который необходимо удалить.</param>
        void Remove(TNode node);

        /// <summary>
        /// Метод возвращает узел по его id.
        /// </summary>
        /// <param name="id">Идентификатор узла.</param>
        /// <returns></returns>
        TNode GetNodeById(int id);

        /// <summary>
        /// Метод возвращает узел по его данным.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        TNode GetNodeByData(TNodeType data);
    }
}
